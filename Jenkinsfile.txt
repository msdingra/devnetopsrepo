pipeline {
    agent any
    stages {
        stage('Get Playbook') {
            steps {
                git 'https://gitlab.com/msdingra/devnetopscicdpipeline.git'
            }
        }
            stage('Playbook Sanity test') {
                    steps {
                        sh 'ansible-lint -x 201,403 playbook_httpd.yml'
                    }
        }                    
        stage('Ansible Run playbook') {
                    steps {
                            sh 'ansible-playbook playbook_httpd.yml'
                    }
        }
    }
}
